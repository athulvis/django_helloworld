from django.db import models

# Create your models here.
class Post(models.Model): #we have created a database model called Post
    text = models.TextField() # text is the database field in that model with type of content, TextField

    def __str__(self):
        return self.text[:50]

    
