from django.urls import path

from .views import FormListView, ViewForm

urlpatterns = [
        path('',FormListView.as_view(),name='home'),
        path('entry/',ViewForm.as_view(),name='entry'),
        ]
        
