from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView
from django.views.generic.edit import CreateView
from .models import Post

class FormListView(ListView):
    model = Post
    template_name = 'home.html'
    fields = '__all__'

class ViewForm(CreateView):
    model = Post
    template_name = 'entry.html'
    fields = "__all__" 
