from django.db import models
from django.urls import reverse
# Create your models here.

class Post(models.Model):
    name = models.CharField(max_length=200)
    age = models.PositiveSmallIntegerField()
    salary = models.PositiveIntegerField()

    def __main__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('home')
